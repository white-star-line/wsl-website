import React from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";

function TripConfirm(){
  return (
      <div>
        <h3>Trip Confirmation Page</h3>
        	<Button component={ Link } variant="body2" to="/">
                Home
            </Button>
      </div>
    );
}

export default TripConfirm;