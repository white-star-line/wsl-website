import React from "react";
import "./App.css";
import Home from "./Home";
import Login from "./SignIn";
import Registration from "./SignUp";
import ShipDetails from "./ShipDetails";
import Booking from "./Booking";
import TripConfirm from "./TripConfirm";
import EmbarkMap from "./EmbarkMap";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";

function App() {
    return (
      <div className="App">
        <Router>
          <Header />
            <Switch>
              <Route exact path="/" component={Home}></Route>
              <Route path="/login" component={Login}></Route>
              <Route path="/registration" component={Registration}></Route>
              <Route path="/shipdetails" component={ShipDetails}></Route>
              <Route path="/booking" component={Booking}></Route>
              <Route path="/tripconfirm" component={TripConfirm}></Route>
              <Route path="/embarkmap" component={EmbarkMap}></Route>
            </Switch>
          <Footer />
        </Router>
      </div>  
    );
}

export default App;
