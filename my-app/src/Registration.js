import React from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';

function Registration(){
  return (
      <div>
        <h3>Register an account</h3>
	        <Button component={ Link } variant="body2" to="/">
                Home
            </Button>
            <Button component={ Link } variant="body2" to="/login">
                Login
            </Button>
         	<form noValidate autoComplete="off">
	     		<TextField id="First-Name" label="First Name" variant="outlined" />
	     		<br></br>
	     		<TextField id="Last-Name" label="Last Name" variant="outlined" />
	     		<br></br>
	     		<TextField id="Email" label="Email" variant="outlined" />
	     		<br></br>
	     		<TextField id="outlined-basic" label="Password" variant="outlined" type="password" />
				<br></br>
				<Button variant="contained">
                  	Sign Up
                </Button>
			</form>
      </div>
    );
}

export default Registration;