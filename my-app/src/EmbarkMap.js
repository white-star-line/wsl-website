import React,
{ Component } from 'react';
import Button from "@material-ui/core/Button";
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
 
const mapBox = {
    height: '400px',
    float: 'left',
}

const mapStyle = { //map box
  width: '100%',
  height: '400px'
};

const infoStyle = { //infoWindow

}

const infoHeaderStyle = {
  width: '100%',
  margin: '0',
}

const sidePanel = {
    float: 'left',
    //width: '200px',
    height: '400px',
}

const clearFloat = {
    clear: 'both',
}

const infoTitle = {
    
}

const infoText = {
    textAlign: 'left',
}

const infoLink = {
    
}

const zoom = 3;

let points = []; //all points of port

points[0] = {
  lat: 26.08554389411854,
  lng: -80.11613214053376
}
points[1] = {
  lat: 30.40699538175174,
  lng: -81.57884677335419
}
points[2] = {
  lat: 25.778791866932902,
  lng: -80.17787664461622
}
points[3] = {
  lat: 28.403798533448022,
  lng: -80.61258897339856
}
points[4] = {
  lat: 26.770738667691536,
  lng: -80.05176154459643
}
points[5] = {
  lat: 27.950358773257683,
  lng: -82.44506880224446
}
points[6] = {
  lat:  30.69074465116986,
  lng: -88.03886433102016
}
points[7] = {
  lat: 29.93993259661527,
  lng: -90.06145567336479
}
points[8] = {
  lat: 29.308201048952853,
  lng: -94.79748120221508
}


        //variables
        let shipList; //The left list div
        let listItems = []; //list items
        let ships = [];

        let markers = []; //markers on the map
        let infoWindows = []; //infoWindows that appear above markers
        
        //creating ship data
        function shipData() {
            this.name = ""; //name of the ship
            this.city = ""; //name of city of embarkation
            this.point = null; //a point of latitude and longitude of embarkation
            
            this.summary = ""; //summary of trip
            this.link = ""; //link to ship's webpage
            
            this.setAllData = function(name, city, point, summary, link)
            {
                this.name = name; 
                this.city = city;
                this.point = point;
                this.summary = summary;
                this.link = link;
            }
        }
        
            const numberOfShips = 9;
            for (let i = 0; i < numberOfShips; i++) {
                let s = new shipData();
                ships.push(s);
            }
            
            let link = "#"; //dummy link

            //Ship 1
            let sum = "The last surviving member of the White Star Line fleet, the Nomadic has been restored to modern luxury cruise liner standards and prepped for new voyages. Built in the United Kingdom, it carries the inspiring image of British naval superiority. The Nomadic may be smaller than other cruise liners, but makes up for it with an extravagant interior."
            ships[0].setAllData("SS Nomadic", "Fort Lauderdale", points[0], sum, link);
            
            //Ship 2
            sum = "This world-renowned ship with a tragic past has been upgraded with state-of-the-art amenities and brought up to modern standards for luxury cruise ships. The largest passenger ship in the world at the time of construction, the Titanic inspired a film that curiously cost more to produce than the ship itself cost to build.";
            ships[1].setAllData("RMS Titanic", "Jacksonville", points[1], sum, link);
            
            //Ship 3
            sum = "Being the first of its class, and having served as a trooper carrier during World War I, the RMS Olympic boasts a colorful history and a solid reputation. Known as “Old Reliable” back in the day, this grand vessel inspired many a Hollywood film with its extravagant design featuring a grand staircase and fanciful upholstery.";
            ships[2].setAllData("RMS Olympic", "Miami", points[2], sum, link);
            
            //Ship 4
            sum = "Titanic’s sister ship, and a vessel with a multifaceted past, the Britannic played the role of a World War I hospital ship, a war ship, and a Royal Mail steamer at different points throughout its original life cycle, until it had the misfortune of running afoul of a mine off the coast of Greece in 1916. Now rebuilt better than ever for your luxury cruise needs.";
            ships[3].setAllData("RMS Britannic", "Port Canaveral", points[3], sum, link);
            
            //Ship 5
            sum = "Known for being the last steam ship to sail the high seas, the Laurentic was also the last White Star Line ship to sink to the briny depths. And while the Laurentic may have a history of serving as an armed merchant cruiser in World War II, you wouldn’t know it now, as it joins the rest of our vessels as a luxury liner par excellence.";
            ships[4].setAllData("SS Laurentic", "Port of Palm Beach", points[4], sum, link);
            
            //Ship 6
            sum = "The last ship built by the original White Star Line, the Georgic replaced the Olympic in 1933 and was known as the largest British motorship in 1931. Of course, as Titanic’s sister ship, the Georgic has much to live up to, and now with all of our recent additions and improvements can match any of our ships in luxury and reliability.";
            ships[5].setAllData("MV Georgic", "Tampa", points[5], sum, link);

            //Ship 7
            sum = "This ship has world class dining to provide quality quisine while at sea. It has a palm court, lounge, smoke room, gym, 1st class dining saloon, and 2nd class dining saloon.";
            ships[6].setAllData("SS Pennland", "Mobile", points[6], sum, link);

            //Ship 8
            sum = "This ship has world class dining to provide quality quisine while at sea. It has a palm court, lounge, smoke room, gym, 1st class dining saloon, and 2nd class dining saloon.";
            ships[7].setAllData("SS Albertic", "New Orleans", points[7], sum, link);

            //Ship 9
            sum = "Known for being the largest motor ship in the British fleet when originally constructed, the MV Britannic served as a troop carrier in World War II and made its final voyage in 1960, until now. Renovated and strengthened, it now sails under the White Star Line banner once again, serving thousands of our valued customers every year.";
            ships[8].setAllData("MV Britannic", "Galveston", points[8], sum, link);


export class MapContainer extends Component {
  state = {
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {},
  };

  onMarkerClick = (props, marker, e) =>
  this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });

  onMapClicked = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  };

  windowHasClosed = (props) => {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
  }
    
  render() {
    return (
      <div>
        <div>
          <h3>Ship Embarkations</h3>
        </div>
        <div style={sidePanel}>
          
        </div>
        <div style={mapBox}>
        <Map style={mapStyle}
        google={this.props.google}
        onClick={this.onMapClicked}
        zoom={zoom} >
        <Marker onClick={this.onMarkerClick}
                name={ships[0].name}
                name={ships[0].city}
                sum={ships[0].summary}
                link={ships[0].link}
                position={points[0]}/>
        <Marker onClick={this.onMarkerClick}
                name={ships[1].name}
                city={ships[1].city}
                sum={ships[1].summary}
                link={ships[1].link}
                position={points[1]} />
        <Marker onClick={this.onMarkerClick}
                name={ships[2].name}
                city={ships[2].city}
                sum={ships[2].summary}
                link={ships[2].link}
                position={points[2]} />
        <Marker onClick={this.onMarkerClick}
                name={ships[3].name}
                city={ships[3].city}
                sum={ships[3].summary}
                link={ships[3].link}
                position={points[3]} />
        <Marker onClick={this.onMarkerClick}
                name={ships[4].name}
                city={ships[4].city}
                sum={ships[4].summary}
                link={ships[4].link}
                position={points[4]} />
        <Marker onClick={this.onMarkerClick}
                name={ships[5].name}
                city={ships[5].city}
                sum={ships[5].summary}
                link={ships[5].link}
                position={points[5]} />
        <Marker onClick={this.onMarkerClick}
                name={ships[6].name}
                city={ships[6].city}
                sum={ships[6].summary}
                link={ships[6].link}
                position={points[6]} />
        <Marker onClick={this.onMarkerClick}
                name={ships[7].name}
                city={ships[7].city}
                sum={ships[7].summary}
                link={ships[7].link}
                position={points[7]} />
        <Marker onClick={this.onMarkerClick}
                name={ships[8].name}
                city={ships[8].city}
                sum={ships[8].summary}
                link={ships[8].link}
                position={points[8]} />
        {this.generateMarker}
        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}
          onClose={this.windowHasClosed}>
            <div style={infoStyle}>
              <h2 style={infoHeaderStyle}>{this.state.selectedPlace.name}</h2>
              <p style={infoText}>Embarkation Port: {this.state.selectedPlace.city}</p>
              <p style={infoText}>{this.state.selectedPlace.sum}</p>
              <a style={infoLink} href={this.state.selectedPlace.link}>Learn more about this ship</a>
            </div>
        </InfoWindow>
      </Map>
        </div>
        <div style={clearFloat}></div>
        </div>
    )
  }
}
 
export default GoogleApiWrapper({
  apiKey: ("AIzaSyBhl9XkeK6AfCBy6C2h4rTi7hbaoue5pWs")
})(MapContainer)