import React from "react";
import ShipBlock from "./components/ShipBlock";
import SlideShow from "./components/SlideShow";
import PackageCard from "./components/PackageCard"

function Home() {
  return (      
      <div>
          <SlideShow/>
        	<PackageCard/>
      </div>
    );
}

export default Home;