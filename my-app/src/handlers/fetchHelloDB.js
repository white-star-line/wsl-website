



export async function fetchHelloBD() {
	const DB_URL = 'https://whitestar-api.tedchang2010.workers.dev/hellodb';
	const response = await fetch(DB_URL);
	console.log("**** fetching: " + DB_URL);
	const data = await response.json();
	return JSON.stringify(data);
}