



export async function fetchAllShips() {
	const DB_URL = 'https://whitestar-api.tedchang2010.workers.dev/getships';
	const response = await fetch(DB_URL);
	console.log("**** fetching: " + DB_URL);
	const data = await response.json();
	console.log("**** fetched: " + JSON.stringify(data))
	return JSON.stringify(data);
}