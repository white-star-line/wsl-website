import React,
{ Component } from 'react';
import { Link } from "react-router-dom";
import PlanTripButton from "./PlanTripBottun";


const headerStyles = { //Footer CSS
  width: 'auto',
  height: '60px',
  padding: '0px',
  backgroundColor: 'ghostwhite',
};

const copyrightTitleStyles = {
    height: '100px',
    lineHeight: '100px', //same as footer height
    textAlign: 'center',
    verticalAlign: 'middle',
}

const containerStyles = {
    
}

const menuLogoStyle = {
    float: 'left',
    marginTop: '5px',
    marginRight: '0px',
    marginBottom: '0px',
    marginLeft: '20px',
}

const menuItemStyle = {
    float: 'right',
    padding: '20px',
    textDecoration: 'none',
    color: 'black',
    fontSize: '13px',
}

class Header extends Component {
    state = {  }
    render() { 
        return (
            <header style={headerStyles}>
               <div>
                <div style={menuLogoStyle}>
                   <Link to="/">
                    <img src={process.env.PUBLIC_URL + '/img/wsl-logo.svg'} width="200px" height="50"/>
                    </Link>
                </div>
            <Link style={menuItemStyle} to="/login">
                LOGIN 
            </Link>
            <PlanTripButton />
                            </div>        
            </header> 
        );
    }
}
 
export default Header;