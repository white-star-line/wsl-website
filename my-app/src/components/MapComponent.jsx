import React from 'react'
import { GoogleMap, useJsApiLoader } from '@react-google-maps/api';
import { Marker } from '@react-google-maps/api';


const containerStyle = { //map box
  width: 'auto',
  height: '400px'
};

const center = { //where the map starts on load
  lat: -3.745,
  lng: -38.523
};

const zoom = 4; //the default zoom level of the map on load. Not needed if fitBounds

let points = []; //all points of port

points[0] = {
  lat: 26.08554389411854,
  lng: -80.11613214053376
}
points[1] = {
  lat: 30.40699538175174,
  lng: -81.57884677335419
}
points[2] = {
  lat: 25.778791866932902,
  lng: -80.17787664461622
}
points[3] = {
  lat: 28.403798533448022,
  lng: -80.61258897339856
}
points[4] = {
  lat: 26.770738667691536,
  lng: -80.05176154459643
}
points[5] = {
  lat: 27.950358773257683,
  lng: -82.44506880224446
}
points[6] = {
  lat:  30.69074465116986,
  lng: -88.03886433102016
}
points[7] = {
  lat: 29.93993259661527,
  lng: -90.06145567336479
}
points[8] = {
  lat: 29.308201048952853,
  lng: -94.79748120221508
}



function MapComponent() {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: "AIzaSyBhl9XkeK6AfCBy6C2h4rTi7hbaoue5pWs"
  })

  const [map, setMap] = React.useState(null)
  
  const onLoad = React.useCallback(function callback(map) {
    
  var bounds = new window.google.maps.LatLngBounds();
  bounds.extend(points[1]);
  bounds.extend(points[2]);
  //map.fitBounds(bounds);
      
    setMap(map)
  }, [])

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null)
  }, [])

  return isLoaded ? (
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={points[1]}
        zoom={zoom}
        onLoad={onLoad}
        onUnmount={onUnmount}
      >
        { /* Child components, such as markers, info windows, etc. */ }
         <Marker
            onLoad={onLoad}
            position={points[0]}
          />
          <Marker
            onLoad={onLoad}
            position={points[1]}
          />
          <Marker
            onLoad={onLoad}
            position={points[2]}
          />
          <Marker
            onLoad={onLoad}
            position={points[3]}
          />
          <Marker
            onLoad={onLoad}
            position={points[4]}
          />
          <Marker
            onLoad={onLoad}
            position={points[5]}
          />
          <Marker
            onLoad={onLoad}
            position={points[6]}
          />
          <Marker
            onLoad={onLoad}
            position={points[7]}
          />
          <Marker
            onLoad={onLoad}
            position={points[8]}
          />
      </GoogleMap>
  ) : <></>
}

export default React.memo(MapComponent)