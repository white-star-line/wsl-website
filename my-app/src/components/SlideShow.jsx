import React,
{ Component } from 'react';
import { Slide } from 'react-slideshow-image';

const slideImages = [
process.env.PUBLIC_URL + '/img/Main-Page-Hero-Image-faded.jpg',
process.env.PUBLIC_URL + '/img/leonardo-yip-n7GPvDOZUB8-unsplash-faded.jpg',
process.env.PUBLIC_URL + '/img/reynier-carl-kqZH-B7IX7o-unsplash-faded.jpg',
process.env.PUBLIC_URL + '/img/arun-sharma-2BWsE-TxgFE-unsplash-faded.jpg',
process.env.PUBLIC_URL + '/img/leonardo-yip-n7GPvDOZUB8-unsplash-faded.jpg',
];

const testStyle = {
     flexDirection: 'column',
}

class ShipBlockAnimated extends Component {

    render() { 
        return (  
            <div class="ShipBlockAnimated">
                <Slide easing="ease" arrows={false} scale={10}>
                    <div className="each-slide">
                        <div style={testStyle} style={{'backgroundImage': `url(${slideImages[0]})`}}>
                           <div style={testStyle}>
                            <span>Luxury is Back!</span>
                            <br/>
                            <p>For decades, the name White Star Lines has been synonymous with the highest luxury experience on the seven seas. 
                                That experience is back in these fine cruises with only the best amenities for the most discerning tastes.</p>
                            </div>
                        </div>
                    </div>
                    <div className="each-slide">
                        <div style={testStyle} style={{'backgroundImage': `url(${slideImages[1]})`}}>
                         <div style={testStyle}>
                            <span>Luxury is Back!</span>
                                 <br/>
                                <p>For decades, the name White Star Lines has been synonymous with the highest luxury experience on the seven seas. 
                                    That experience is back in these fine cruises with only the best amenities for the most discerning tastes.</p>
                            </div>
                        </div>
                    </div>
                    <div className="each-slide">
                        <div style={testStyle}  style={{'backgroundImage': `url(${slideImages[2]})`}}>
                            <div style={testStyle}>
                                <span>Luxury is Back!</span>
                                <br/>
                                <p>For decades, the name White Star Lines has been synonymous with the highest luxury experience on the seven seas. 
                                    That experience is back in these fine cruises with only the best amenities for the most discerning tastes.</p>
                            </div>
                        </div>
                    </div>
                    <div className="each-slide">
                        <div style={testStyle}  style={{'backgroundImage': `url(${slideImages[3]})`}}>
                            <div style={testStyle}>
                                <span>Luxury is Back!</span>
                                <br/>
                                <p>For decades, the name White Star Lines has been synonymous with the highest luxury experience on the seven seas. 
                                    That experience is back in these fine cruises with only the best amenities for the most discerning tastes.</p>
                            </div>
                        </div>
                    </div>
                </Slide>
            </div>
        );
    }
}
 
export default ShipBlockAnimated;