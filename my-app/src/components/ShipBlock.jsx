import React,
{ Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

class ShipBlock extends Component {
    RMS_Titanic = {
    image: process.env.PUBLIC_URL + '/img/RMS_Titanic.jpg', 
    description:"The famous ship that was sunk by an iceberg during its maiden voyage, now rebuilt. Upgraded with the state of the art luxury amenities and modern standards, your cruise adventure across the Atlantic.",
    name: "Titanic"};

    render() { 
        return (
            <div class="ShipBlock">
                <img src={this.RMS_Titanic.image} width="200" height="200"/>
                <h3>{this.RMS_Titanic.name}</h3>
                <p>{this.RMS_Titanic.description} </p>
                <a href="/Booking">
                    <Button variant="contained">Book Today!</Button>
                </a>
            </div> 
        );
    }
}
//To be used for the database interaction
//{this.imageLoad}
//{this.shipTitle}
//{this.description}
//
 
export default ShipBlock;