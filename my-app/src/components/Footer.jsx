import React,
{ Component } from 'react';

const footerStyles = { //Footer CSS
  width: 'auto',
  height: '65px',
  margin: 'auto',
    width: '400px',
  backgroundColor: 'ghostwhite',
};

const backgroundWidth = {
    backgroundColor: 'ghostwhite',
}

const copyrightTitleStyles = {
    lineHeight: '0px', //same as footer height
    textAlign: 'left',
    verticalAlign: 'middle',
    color: 'black',
    marginLeft: '20px',
    marginTop: '25px',
    marginBottom: '0px'
}

const secondLine = {
    lineHeight: '0px', //same as footer height
    textAlign: 'left',
    verticalAlign: 'middle',
    color: 'black',
    marginLeft: '20px',
    marginTop: '20px',
    marginTop: '15px'
}

const logoStyle = {
    width: '200px',
    float: 'left',
    textAlign: 'center',
    marginTop: '20px',
}

const floatLeft = {
    float: 'left',
}

const containerStyles = {
    
}

class Footer extends Component {
    state = {  }
    render() { 
        return ( <div style={backgroundWidth}>
            <footer style={footerStyles}>
           <div style={containerStyles}>
           <img style={logoStyle} src={process.env.PUBLIC_URL + '/img/wsl-logo.svg'} />
           <div style={floatLeft}>
            <h6 style={copyrightTitleStyles}>© 2021 White Star Lines</h6>
            <h6 style={secondLine}>All right reserved</h6>
           </div>
            </div>
        </footer> 
                   </div>

        );
    }
}
 
export default Footer;