import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { green } from '@material-ui/core/colors';

const GreenButton = withStyles((theme) => ({
    root: {
        color: theme.palette.getContrastText(green[600]),
        backgroundColor: green[600],
        '&:hover': {
            backgroundColor: green[800],
      },
    },
  }))(Button);
  
const floatRight = {
    float: 'right',
}

  const useStyles = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(1),
    },
  }));

  
  export default function CustomizedButtons() {
    const classes = useStyles();
  
    return (
      <div>
        <a href="/embarkmap">
        <GreenButton style={floatRight} variant="contained" color="primary" className={classes.margin}>
          Plan your next Cruise!
        </GreenButton>
        </a>
      </div>
      );
  }

