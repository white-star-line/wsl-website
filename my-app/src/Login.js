import React from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';

function Login(){
  return (
      <div>
        <h3>Log in</h3>
            <Button component={ Link } variant="body2" to="/registration">
                
            </Button>
            <form noValidate autoComplete="off">
                <TextField id="Email" label="Email" variant="outlined" />
                <br></br>
                <TextField id="outlined-basic" label="Password" variant="outlined" type="password" />
                <br></br>
                <Button variant="contained">
                  Log In
                </Button>
                   
            </form>
      </div>
    );
}

export default Login;