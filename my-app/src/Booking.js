import React from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";

function Booking(){
  return (
      <div>
        <h3>Booking Page</h3>
            <Button component={ Link } variant="body2" to="/tripconfirm">
                Trip Confirmation
            </Button>
      </div>
    );
}

export default Booking;