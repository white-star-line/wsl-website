import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import SlideShow from "./components/SlideShow";
import { fetchAllShips } from "./handlers/fetchAllShips";
    
    
function ShipDetails(){
    const [shipList, setShipList] = useState([]);
    useEffect(() => {
        fetchAllShips().then(
            (value) => setShipList(value)
        )
        // code to run on component mount
        }, [])
    var render = function(props) {
        console.log(props.ships)
        return (
            <div>
              <h3>Ship Details Page</h3>
              <SlideShow/>
            </div>
          );
    }
    return render({ships: shipList})
    
}

export default ShipDetails;