# 👷 `worker-template` Hello World


#### Dependencies Ubuntu
```
apt install npm git
npm i @cloudflare/wrangler -g
cd wsl-website/whitestar-api
npm install
```

A template for kick starting a Cloudflare worker project.

[`index.js`](https://github.com/cloudflare/worker-template/blob/master/index.js) is the content of the Workers script.

#### Wrangler

To generate using [wrangler](https://github.com/cloudflare/wrangler)

```
wrangler generate projectname https://github.com/cloudflare/worker-template
```

Further documentation for Wrangler can be found [here](https://developers.cloudflare.com/workers/tooling/wrangler).


##### Local Testing
1. Running the backend locally.
```bash
wranger dev
```
```
curl -X POST -d "user=dog&password=cow"  http://127.0.0.1:8787/login
```


##### Remote Testing

2. Grabbing the publish
```bash
curl  https://whitestar-api.tedchang2010.workers.dev
```
3. View headers
```bash
curl -i -H 'Origin: http://sample.com' https://whitestar-api.tedchang2010.workers.dev/hellodb
curl -X OPTIONS https://whitestar-api.tedchang2010.workers.dev/hellodb
curl -X OPTIONS -i -H 'Origin: http://sample.com' https://whitestar-api.tedchang2010.workers.dev/hellodb

curl -X POST -d "user=bunny&password=carrot&email=bunny@example.com" https://whitestar-api.tedchang2010.workers.dev/createuser
{"error_message":"OK","error_code":0,"endpoint":"createuser","supportedContentTypes":["application/x-www-form-urlencoded","application/json"]}%                                   
```

#### Frontend Group
Routes

Backend URL: https://whitestar-api.tedchang2010.workers.dev
1. GET
   a. /
   b. /hellodb
2. POST
   a. /login
   b. /testToken
   c. /createuser


/login

* Fields
```json
{ "user": "usernameString", "password": "usernamePassword" }

{"contentType":"application/json",
"error_message":"authenication failed","error_code":0,
"token":"e1c19caa7f1528256983c2f2633182a9fd6daedf21f8c3ea1611940a4ae10ad1",
"endpoint":"login",
"supportedContentTypes":["application/x-www-form-urlencoded","application/json"]}
```
```javascript
// Example POST method implementation:
async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}
postData('https://whitestar-api.tedchang2010.workers.dev/login', { password: "mypassword", user: "myuser"})
  .then(data => {
    console.log(data); // JSON data parsed by `data.json()` call
  });
```
https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

/createuser

* Fields
```json
{
  "user":"dummy",
  "password":"tummy",
  "email": "dummy@example.com"
}
```

/testToken

* Fields
```json
{
  "user":"bunny",
  "token": "e1c19caa7f1528256983c2f2633182a9fd6daedf21f8c3ea1611940a4ae10ad1"
}
```

#### Key requirements

```bash
wrangler secret put MONGODB_SECRET
```
