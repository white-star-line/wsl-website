const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");

const Router = require('./router')

import { handleLogin } from './post/handleLogin.js';
import { handleTestToken } from './post/handleTestToken.js';
import { handleHelloDynamodb } from './get/handleHelloDynamodb.js';
import { handleAllOption } from './option/handleAllOption.js';
import { handleCreateUser } from './post/handleCreateUser.js';
import { handleGetShips } from './get/handleGetShips.js';

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
/**
 * Respond with hello worker text
 * @param {Request} request
 */

function myCredentialProvider() {
    return {
        // use wrangler secrets to provide these global variables
        accessKeyId: AWS_ACCESS_KEY_ID,
        secretAccessKey: AWS_SECRET_ACCESS_KEY
    }
}
const dbclient = new DynamoDBClient({
    region: "us-east-1",
    credentialDefaultProvider: myCredentialProvider
});




function handler(request) {
    const init = {
        headers: { 'content-type': 'application/json' },
    }
    const body = JSON.stringify({ some: 'json' })
    return new Response(body, init)
}

async function handleRequest(request) {
    const r = new Router()
    // Replace with the appropriate paths and handlers
    r.post('.*/login', request => handleLogin(request, dbclient))
    r.post('.*/testToken', request => handleTestToken(request, dbclient))
    r.get('.*/getships', request => handleGetShips(request, dbclient))
    r.options('.*/getships', request => handleAllOption(request))
    r.post('.*/createuser', request => handleCreateUser(request, dbclient))
    r.get('.*/foo', request => handler(request))
    r.get('.*/hellodb', request => handleHelloDynamodb(request, dbclient))
    r.options('.*/hellodb', request => handleAllOption(request))
    r.options('.*/login', request => handleAllOption(request))
    r.options('.*/testToken', request => handleAllOption(request))
    r.get('/', () => new Response('Hello worker!')) // return a default message for the root route
    const resp = await r.route(request)
    return resp
}
