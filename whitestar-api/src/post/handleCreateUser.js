
const { PutItemCommand } = require("@aws-sdk/client-dynamodb");
import { handlePostBody } from './handlePostBody.js';
import { calculateHash, getRandomInt } from '../utils.js';

/* 
 * AWS db put payload
 *   {
 *   "$metadata": {
 *       "httpStatusCode": 200,
 *       "requestId": "5P17CVH3452R9DL9NKTPLNTF43VV4KQNSO5AEMVJF66Q9ASUAAJG",
 *       "attempts": 1,
 *       "totalRetryDelay": 0
 *   }
 *   }
 */
async function dynamoCreateUser(dbclient, userinfo) {
    let salt = getRandomInt(10000000).toString(16);
    // replace with your table name and key as appropriate
    let hash = calculateHash(userinfo["password"], salt);
    const put = new PutItemCommand({
        TableName: "user",
        Item: {
            "username": {S: userinfo["username"]},
            "email": {S: userinfo["email"]},
            "sha256hash": {S: hash},
            "salt" : {S: salt}
        }
    });
    const results = await dbclient.send(put);
    return results["$metadata"];
}

export async function handleCreateUser(request, dbclient) {
	const { headers } = request;
	const contentType = headers.get("content-type") || "";
	const init = {
		headers: { 
			'content-type': 'application/json',
            "content-length": '2048',
			'Access-Control-Allow-Origin': '*' ,
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
			"Access-Control-Allow-Headers" : "Content-Type"
		}
	}
	if (contentType.includes("application/json") || contentType.includes("form")) {
		var args = await handlePostBody(request);
	} else {
		let body = JSON.stringify({
			contentType: headers.get("content-type"), 
			error_message: "contentType unsupported",
			error_code: 400,
			endpoint: "createuser",
			supportedContentTypes: ["application/x-www-form-urlencoded", "application/json"]
		});
		return new Response(body, init)
	}
	if( args.hasOwnProperty('user') && args.hasOwnProperty('password') && args.hasOwnProperty('email')) {
        let dynamoResult = await dynamoCreateUser(dbclient, { "username" : args['user'], "password": args['password'], "email": args['email']})
        if (dynamoResult['httpStatusCode'] === 200) {
            var body = JSON.stringify({
                error_message: "OK",
                error_code: 0,
                endpoint: "createuser",
                supportedContentTypes: ["application/x-www-form-urlencoded", "application/json"]
            });
            init["status"] = 200
        } else {
            var body = JSON.stringify({
                contentType: headers.get("content-type"), 
                error_message: "Create User Failed",
                error_code: 402,
                endpoint: "createuser",
                arguments: args,
                supportedContentTypes: ["application/x-www-form-urlencoded", "application/json"]
            });
        }
		return new Response(body, init)
	} else {
		var body = JSON.stringify({
			contentType: headers.get("content-type"), 
			error_message: "user and password argument not found",
			error_code: 401,
			endpoint: "createuser",
			arguments: args
		})
	}
	return new Response(body, init)

}