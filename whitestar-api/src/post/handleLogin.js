const { PutItemCommand, GetItemCommand } = require("@aws-sdk/client-dynamodb");
import { handlePostBody } from './handlePostBody.js';
import { calculateHash, getRandomInt } from '../utils.js';

async function dynamoLogin(userinfo, dbclient) {
    // replace with your table name and key as appropriate
    const get = new GetItemCommand({
        TableName: "user",
        Key: {
            "username": {S: userinfo["username"]}
        }
    });
    const results = await dbclient.send(get);
	console.log(results)
    return results.Item;
}
async function dynamoCreateToken(userinfo, dbclient) {
	let token = calculateHash("whatever", getRandomInt(1000000000000000).toString(16))
    // replace with your table name and key as appropriate
	const put = new PutItemCommand({
        TableName: "session",
        Item: {
			"token": {S: token},
            "username": {S: userinfo["username"]}
        }
    });
    const results = await dbclient.send(put);
	console.log(results)
    return token;
}


export async function handleLogin(request, dbclient) {
	const { headers } = request;
	const contentType = headers.get("content-type") || "";
	const init = {
		headers: { 
			'content-type': 'application/json',
			'Access-Control-Allow-Origin': '*' ,
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
			"Access-Control-Allow-Headers" : "Content-Type",
			"status": 400
		}
	}
	if (contentType.includes("application/json") || contentType.includes("form")) {
		var args = await handlePostBody(request);
	} else {
		let body = JSON.stringify({
			contentType: headers.get("content-type"), 
			error_message: "contentType unsupported",
			error_code: 200,
			endpoint: "login",
			supportedContentTypes: ["application/x-www-form-urlencoded", "application/json"]
		});
		return new Response(body, init)
	}
	if( args.hasOwnProperty('user') && args.hasOwnProperty('password')) {
		let result = await dynamoLogin({"username" : args["user"]}, dbclient);
		let salt = result["salt"].S;
		let expectedHash = result["sha256hash"].S;
		let inputHash = calculateHash(args["password"], salt);
		if( inputHash === expectedHash) {
			let token = await dynamoCreateToken({"username" : args["user"]}, dbclient)
			var body = JSON.stringify({
				contentType: headers.get("content-type"), 
				error_message: "authenication failed",
				error_code: 0,
				token: token,
				endpoint: "login",
				supportedContentTypes: ["application/x-www-form-urlencoded", "application/json"]
			});
			init["status"] = 200
		} else {
			var body = JSON.stringify({
				contentType: headers.get("content-type"), 
				error_message: "authenication failed",
				error_code: 202,
				endpoint: "login",
				supportedContentTypes: ["application/x-www-form-urlencoded", "application/json"]
			});
		}
		return new Response(body, init)
	} else {
		var body = JSON.stringify({
			contentType: headers.get("content-type"), 
			error_message: "user and password argument not found",
			error_code: 201,
			endpoint: "login",
		})
	}
	return new Response(body, init)

}