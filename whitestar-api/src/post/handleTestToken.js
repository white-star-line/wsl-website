const { GetItemCommand } = require("@aws-sdk/client-dynamodb");
import { handlePostBody } from './handlePostBody.js';


async function getDynamoToken(userinfo, dbclient) {
    // replace with your table name and key as appropriate
    const get = new GetItemCommand({
        TableName: "session",
        Key: {
            "token": {S: userinfo["token"]}
        }
    });
	try {
    	var results = await dbclient.send(get);
	} catch (err) {
		throw "session not found";
	}
	console.log(results)
    return results.Item;
}

export async function handleTestToken(request, dbclient) {
	const { headers } = request;
	const contentType = headers.get("content-type") || "";
	const init = {
		headers: { 
			'content-type': 'application/json',
			'Access-Control-Allow-Origin': '*' ,
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
			"Access-Control-Allow-Headers" : "Content-Type",
			"status" : 400
		 }
	}
	if (contentType.includes("application/json") || contentType.includes("form")) {
		var args = await handlePostBody(request);
	} else {
		let body = JSON.stringify({
			contentType: headers.get("content-type"), 
			error_message: "contentType unsupported",
			error_code: 300,
			endpoint: "testToken",
			supportedContentTypes: ["application/x-www-form-urlencoded", "application/json"]
		});
		return new Response(body, init)
	}
	if( args.hasOwnProperty('user') && args.hasOwnProperty('token')) {
		let result = await getDynamoToken({"token" : args["token"]}, dbclient);
		if( args["user"] === result["username"].S) {
			var body = JSON.stringify({
				contentType: headers.get("content-type"), 
				error_message: "OK",
				error_code: 0,
				endpoint: "testToken",
				username: result["username"].S,
			});
		} else {
			var body = JSON.stringify({
				contentType: headers.get("content-type"), 
				error_message: "token authenication failed",
				error_code: 302,
				endpoint: "testToken",
				supportedContentTypes: ["application/x-www-form-urlencoded", "application/json"]
			});
		}
		return new Response(body, init)
	} else {
		var body = JSON.stringify({
			contentType: headers.get("content-type"), 
			error_message: "token not found",
			error_code: 301,
			endpoint: "testToken",
		})
	}
	return new Response(body, init)
}