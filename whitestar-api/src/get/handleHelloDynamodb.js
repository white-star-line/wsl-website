
const { GetItemCommand } = require("@aws-sdk/client-dynamodb");


async function dynamoExample(dbclient) {

    // replace with your table name and key as appropriate
    const get = new GetItemCommand({
        TableName: "hello",
        Key: {
            "key": {S: "hello"}
        }
    });
    const results = await dbclient.send(get);
    return results.Item;
}
export async function handleHelloDynamodb(request, dbclient) {
    const init = {
        headers: { 'content-type': 'application/json',
			'Access-Control-Allow-Origin': '*' ,
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
			"Access-Control-Allow-Headers" : "Content-Type"  },
    }
	const result = await dynamoExample(dbclient);
    const body = JSON.stringify(result)
    return new Response(body, init)
}
