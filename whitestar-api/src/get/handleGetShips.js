const { BatchGetItemCommand, QueryCommand, GetItemCommand } = require("@aws-sdk/client-dynamodb");
const { marshall, unmarshall } = require("@aws-sdk/util-dynamodb");

/*****
 * const get = BatchGetItemCommand({
        "RequestItems" : {
            "ship" : {
                "Keys": [
                    {"the-ship": {"S": "MV-Britannic"}},
                    {"the-ship": {"S": "MV-Georgic"}},
                    {"the-ship": {"S": "RMS-Britannic"}},
                    {"the-ship": {"S": "RMS-Olympic"}},
                    {"the-ship": {"S": "RMS-Britannic"}},
                    {"the-ship": {"S": "SS-Albertic"}},
                    {"the-ship": {"S": "SS-Laurentic"}},
                    {"the-ship": {"S": "SS-Nomadic"}},
                    {"the-ship": {"S": "SS-Pennland"}},
                    {"the-ship": {"S": "MV-Britannic"}}
                ]
            }
        }
    });
 */
async function dynamoGetShips(dbclient) {
    const ship_keys = [
        "MV-Britannic",
        "MV-Georgic",
        "RMS-Britannic",
        "RMS-Olympic",
        "RMS-Britannic",
        "SS-Albertic",
        "SS-Laurentic",
        "SS-Nomadic",
        "SS-Pennland",
        "MV-Britannic"
    ]
    function makeGet(key) {
        const get = new GetItemCommand({
            TableName: "ship",
            Key: {
                "the-ship": {S: key}
            }
        });
        return get
    }
    
    var accumlator = []
    for(let i = 0; i < ship_keys.length; i++) {
        let item = await dbclient.send(makeGet(ship_keys[i]))
        console.log(item["Item"])
        accumlator.push(unmarshall(item["Item"]))
    }
    console.log(accumlator)
    return accumlator;
}

export async function handleGetShips(request, dbclient) {
    const init = {
        headers: { 
            'content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "Access-Control-Allow-Headers" : "Content-Type" 
        }
    };
    const result = await dynamoGetShips(dbclient);
    const body = JSON.stringify(result);
    console.log(result);
    return new Response(body, init);
}