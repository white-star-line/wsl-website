export async function handleAllOption(request) {
	const init = {
		headers: { 
			'Content-type': 'application/json',
			'Access-Control-Allow-Origin': '*' ,
			"Access-Control-Allow-Methods": "OPTIONS,POST,PUT,GET",
			"Access-Control-Allow-Headers" : "Content-Type, Origin, x-requested-with",
			"Accept": "application/json,application/x-www-form-urlencoded",
			"status" : 200
		}
	}
	return new Response(JSON.stringify({status: "OK"}), init);
}