//https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest
export function calculateHash(password, salt) {
	var hash = require('hash.js')
	let saltypassword = salt + password;
	let h = hash.sha256().update( saltypassword).digest('hex')
	return h;
}

function ab2str(buf) {
	return String.fromCharCode.apply(null, new Uint16Array(buf));
  }

export function getRandomInt(max) {
	return Math.floor(Math.random() * max);
}